# Flask with Docker

## Testing

`sh test.sh`

## Running

`sh run.sh`

## Production

`sh prod.sh`

## Local development workflow

From a virtual environment run:

`python -m pip install -r requirements/dev.txt`

### Running the server

`python app/run.py`

### Code formatting with black

`black .`
