#!/usr/bin/env bash
set -e

docker-compose -f docker-compose.dev.yml build
docker-compose -f docker-compose.dev.yml run app -m pytest .
