from run import app


def test_index():
    app.testing = True
    client = app.test_client()

    res = client.get("/")

    assert res.status_code == 200
    assert res.data.decode("ASCII") == "Index"
