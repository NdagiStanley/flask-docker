FROM python:alpine
RUN pip install --upgrade pip

WORKDIR /usr/src/app

COPY requirements.txt ./
COPY requirements/ requirements/
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

# Note the WORKDIR
WORKDIR "app"
ENTRYPOINT ["python"]

# Note since WORKDIR is at the 'app' folder hence the command is 'python run.py'
# Every other command in 'docker run -it' will be run at the 'app' folder
CMD ["run.py"]
